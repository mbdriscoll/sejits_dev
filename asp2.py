"""
asp2.py

Demonstration of SEJITS techniques without Codepy. Requires Python 3 for
function annotations (see http://www.python.org/dev/peps/pep-3107/), an
external compiler 'clang' for C->LLVM translation, and llvmpy for jit
compilation. Doesn't require codepy, cgen, or boost-python!
"""

# ---------------------------------------------------------------------------
# program to compile

def py_fib(x :int) -> int:
  if x < 2:
    return x
  else:
    return py_fib(x-2) + py_fib(x-1)

# ---------------------------------------------------------------------------
# parse python program

import ast
import inspect

prog_txt = inspect.getsource(py_fib)
py_ast = ast.parse(prog_txt)
print("Python program:\n%s" % prog_txt)

# ---------------------------------------------------------------------------
# transform py_ast to c_ast

c_ast = py_ast

# ---------------------------------------------------------------------------
# unparse c_ast

import ast

BINOP_TO_STR = {
  ast.Add:    "+",
  ast.Lt:     "<",
  ast.Sub:    "-",
}

class Unparser(ast.NodeVisitor):
  def __init__(self):
    self._indent = 0

  def _tabs(self):
    return "  " * self._indent

  def _unparse_block(self, node):
    self._indent += 1
    inner = self._tabs()
    if isinstance(node, list):
      inner += (";\n" + self._tabs()).join(map(self.visit, node))
    else:
      inner += self.visit(node)
    inner += ";"
    self._indent -= 1
    return "{\n%s\n%s}\n" % (inner, self._tabs())

  def visit_FunctionDef(self, node):
    rettype = self.visit(node.returns)
    arglist = ", ".join(map(self.visit, node.args.args))
    body = self._unparse_block(node.body)
    return "%s %s(%s) %s" % (rettype, node.name, arglist, body)

  def visit_Module(self, node):
    return "\n".join(map(self.visit, node.body))

  def visit_If(self, node):
    test = self.visit(node.test)
    body = self._unparse_block(node.body)
    s = "if (%s) %s" % (test, body)
    if node.orelse:
      s += self._tabs() + "else %s" % self._unparse_block(node.orelse)
    return s

  def visit_Return(self, node):
    return "return %s" % self.visit(node.value)

  def visit_BinOp(self, node):
    lhs = self.visit(node.left)
    op = BINOP_TO_STR[type(node.op)]
    rhs = self.visit(node.right)
    return "%s %s %s" % (lhs, op, rhs)

  def visit_Call(self, node):
    func = self.visit(node.func)
    args = ", ".join(map(self.visit, node.args))
    return "%s(%s)" % (func, args)

  def visit_Num(self, node):
    return node.n

  def visit_Name(self, node):
    return node.id

  def visit_Compare(self, node):
    assert len(node.ops) == 1, "Unparser only supports binary comparisons"
    left = self.visit(node.left)
    op = BINOP_TO_STR[type(node.ops[0])]
    right = self.visit(node.comparators[0])
    return "%s %s %s" % (left, op, right)

  def visit_arg(self, node):
    ty = self.visit(node.annotation)
    return "%s %s" % (ty, node.arg)

program = Unparser().visit(c_ast)
print("C Program:\n%s" % program)

# ---------------------------------------------------------------------------
# make a temp directory for source and bitcode

import os
import tempfile
comp_dir = tempfile.mkdtemp(prefix="sejits-", dir=tempfile.gettempdir())
c_src_file = os.path.join(comp_dir, "generated.0.c")
ll_bc_file = os.path.join(comp_dir, "generated.1.bc")

# ---------------------------------------------------------------------------
# write program to temp file

with open(c_src_file, 'w') as c_file:
  c_file.write(program)

# ---------------------------------------------------------------------------
# compile it to llvm bitcode

import subprocess
compile_cmd = "clang -emit-llvm -o %s -c %s" % (ll_bc_file, c_src_file)
subprocess.check_call(compile_cmd, shell=True)

# ---------------------------------------------------------------------------
# load and compile llvm bitcode

from llvm.core import Module
with open(ll_bc_file, 'rb') as bc:
  module = Module.from_bitcode(bc)
# print("LLVM code:\n%s" % module)
ll_function = module.get_function_named("py_fib")

# ---------------------------------------------------------------------------
# create execution engine and get function pointer

from llvm.ee import EngineBuilder
ee = EngineBuilder.new(module).mcjit(True).create()
c_func_ptr = ee.get_pointer_to_function(ll_function)

# ---------------------------------------------------------------------------
# wrap pointer with ctypes

import ctypes as ct

PYTYPE_TO_CTYPE = {
  int:   ct.c_int,
  float: ct.c_float,
}

def eval_ast(tree):
  return eval(compile(ast.Expression(tree), __name__, 'eval'))

def funcdef_to_ctypes_signature(funcdef):
  """
  Infer ctype function type from ast.
  This should be a method on the FunctionDef ast node.
  """
  assert isinstance(funcdef, ast.FunctionDef)
  sig = [funcdef.returns] + [a.annotation for a in funcdef.args.args]
  return ct.CFUNCTYPE(*[PYTYPE_TO_CTYPE[tyclass] for tyclass in map(eval_ast, sig)])

func_type = funcdef_to_ctypes_signature(c_ast.body[0])
c_fib = func_type(c_func_ptr)

# ---------------------------------------------------------------------------
# call and check jit-compiled function

assert c_fib(1) == 1
assert c_fib(2) == 1
assert c_fib(3) == 2
assert c_fib(4) == 3
assert c_fib(5) == 5
assert c_fib(6) == 8

# ---------------------------------------------------------------------------
# see how much faster it is

from time import time

class Timer:
  def __init__(self, name):
    self._name = name
  def __enter__(self):
    self._start = time()
    return self
  def __exit__(self, *args):
    self._duration = time() - self._start
    print("  %s: %d ms" % (self._name, 1000.0 * self._duration))
  def __truediv__(self, o):
    return self._duration / o._duration

for n in range(8, 33, 8):
  print ("Testing fib(%d)" % n)
  with Timer("c") as c_time:
    c = c_fib(n)
  with Timer("py") as py_time:
    py = py_fib(n)
  assert c == py
  print ("  %.fx faster in c" % (py_time/c_time))
